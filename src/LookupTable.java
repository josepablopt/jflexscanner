import java.util.HashMap;

public class LookupTable extends HashMap<String,ExpTokenType> {

    /**
     * Creates a lookup table, loading all the token types.
     */
    public LookupTable() {
        // Keywords
        this.put( "char", TokenType.CHAR);
        this.put( "while", TokenType.WHILE);
        this.put( "else", TokenType.ELSE);
        this.put( "float", TokenType.FLOAT);
        this.put( "int", TokenType.INT);
        this.put( "if", TokenType.IF);
        this.put( "return", TokenType.RETURN);
        this.put( "void", TokenType.VOID);

        // Symbols
        this.put( "(", TokenType.LPAREN);
        this.put( ")", TokenType.RPAREN);
        this.put( "{", TokenType.LBRACE);
        this.put( "}", TokenType.RBRACE);
        this.put( "[", TokenType.LBRACK);
        this.put( "]", TokenType.RBRACK);
        this.put( ";", TokenType.SEMICOLON);
        this.put( ",", TokenType.COMMA);
        this.put( ".", TokenType.DOT);
        this.put( "=", TokenType.EQ);
        this.put( ">", TokenType.GT);
        this.put( "<", TokenType.LT);
        this.put( "!", TokenType.NOT);
        this.put( "==", TokenType.EQEQ);
        this.put( "<=", TokenType.LTEQ);
        this.put( ">=", TokenType.GTEQ);
        this.put( "!=", TokenType.NOTEQ);
        this.put( "&&", TokenType.AND);
        this.put( "||", TokenType.OROR);
        this.put( "+", TokenType.PLUS);
        this.put( "-", TokenType.MINUS);
        this.put( "*", TokenType.MULT);
        this.put( "/", TokenType.MULT);
    }
}