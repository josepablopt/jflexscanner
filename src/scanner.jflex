/**
 * Starting the jflex This is a simple example of a jflex lexer definition
 */

/* Declarations */


%%

%standalone         /* The produced java file has a main */
%class  MyScannerName   /* Names the produced java file */
%function nextToken /* Renames the yylex() function */
%type   String      /* Defines the return type of the scanning function */
%eofval{
  return null;
%eofval}
/* Patterns */
%{
      LookupTable table = new LookupTable();
      private Symbol symbol(int type) {
          return new JavaSymbol(type, yyline+1, yycolumn+1);
        }

        private Symbol symbol(int type, Object value) {
          return new JavaSymbol(type, yyline+1, yycolumn+1, value);
        }
%}

other         = .
letter        = [A-Za-z]
word          = {letter}+
whitespace    = [ \n\t]
number        = 0 | [1-9][0-9]*  //a number between one and nine followed by zero or more numbers between zero and nine or just a zero.
Comment       =   {NormalComments} | {FullDocumentation}
NormalComments = "/*" [^*] ~"*/" | "/*" "*"+ "/"
FullDocumentation = "/*" "*"+ [^/*] ~"*/"

%%
/* Lexical Rules */

/* Printing a token found that was declared in the class sym and then return it. */


<YYINITIAL> {

  /* keywords */

  "char"                         { return symbol(CHAR); }
  "else"                         { return symbol(ELSE); }
  "float"                        { return symbol(FLOAT); }
  "int"                          { return symbol(INT); }
  "if"                           { return symbol(IF); }
  "return"                       { return symbol(RETURN); }
  "void"                         { return symbol(VOID); }
  "while"                        { return symbol(WHILE); }

  /* symbols */
  "("                            { return symbol(LPAREN); }
  ")"                            { return symbol(RPAREN); }
  "{"                            { return symbol(LBRACE); }
  "}"                            { return symbol(RBRACE); }
  "["                            { return symbol(LBRACK); }
  "]"                            { return symbol(RBRACK); }
  ";"                            { return symbol(SEMICOLON); }
  ","                            { return symbol(COMMA); }
  "."                            { return symbol(DOT); }
  "="                            { return symbol(EQ); }
  ">"                            { return symbol(GT); }
  "<"                            { return symbol(LT); }
  "!"                            { return symbol(NOT); }
  "=="                           { return symbol(EQEQ); }
  "<="                           { return symbol(LTEQ); }
  ">="                           { return symbol(GTEQ); }
  "!="                           { return symbol(NOTEQ); }
  "&&"                           { return symbol(ANDAND); }
  "||"                           { return symbol(OROR); }
  "+"                            { return symbol(PLUS); }
  "-"                            { return symbol(MINUS); }
  "*"                            { return symbol(MULT); }
  "/"                            { return symbol(MULT); }


  /* string literal */
  \"                             { yybegin(STRING); string.setLength(0); }

  /* ignoring comments */
  {Comment}                      { /* ignore */ }

  /* whitespace */
  {WhiteSpace}                   { /* ignore */ }

  /* Words */
  {word}     {
               /** Print out the word that was found. */
               //System.out.println("Found a word: " + yytext());
               return( yytext());
              }
  /* Numbers */
  {number}    {
                /** Print out the number that was found. */
               //System.out.println("Found a number: " + yytext());
               return( yytext());
              }

}



